package com.oredata.utils;

import org.apache.log4j.Logger;

public class LoggerUtil {

    private static LoggerUtil loggerUtil = null;

    private Logger generalLogger;
    private Logger tableOutputLogger;

    private LoggerUtil() {
        generalLogger = Logger.getLogger("GeneralLogger");
        tableOutputLogger = Logger.getLogger("TableOutputLogger");
    }

    public static LoggerUtil getInstance() {
        if (loggerUtil == null) {
            loggerUtil = new LoggerUtil();
        }
        return loggerUtil;
    }

    public Logger getGeneralLogger() {
        return generalLogger;
    }

    public void setGeneralLogger(Logger generalLogger) {
        this.generalLogger = generalLogger;
    }

    public Logger getTableOutputLogger() {
        return tableOutputLogger;
    }

    public void setTableOutputLogger(Logger tableOutputLogger) {
        this.tableOutputLogger = tableOutputLogger;
    }

}
