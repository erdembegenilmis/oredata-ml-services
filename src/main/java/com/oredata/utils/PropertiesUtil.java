package com.oredata.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;

public class PropertiesUtil {

    private Properties prop;
    private static PropertiesUtil propertiesUtil;

    private PropertiesUtil() {
        prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("/data/Oredata/app/odeabank/application.properties");
            // load a properties file
            prop.load(input);

            LoggerUtil.getInstance().getGeneralLogger().debug("Properties are :");
            for (Object property : prop.keySet()) {
                String key = property.toString();
                LoggerUtil.getInstance().getGeneralLogger().debug(key + " - " + prop.getProperty(key));

            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static synchronized PropertiesUtil getInstance() {
        if (propertiesUtil == null) {
            propertiesUtil = new PropertiesUtil();
        }
        return propertiesUtil;
    }

    public String getProperty(String property, String defaultValue) {
        try {
            String foundProperty = prop.getProperty(property).trim();
            if (StringUtils.isEmpty(foundProperty)) {
                foundProperty = defaultValue;
            }
            return foundProperty;
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public Integer getIntProperty(String property, Integer defaultValue) {
        try {
            return Integer.valueOf(prop.getProperty(property).trim());
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public Boolean getBooleanProperty(String property, Boolean defaultValue) {
        try {
            return Boolean.valueOf(prop.getProperty(property).trim());
        } catch (Exception e) {
            return defaultValue;
        }
    }

}