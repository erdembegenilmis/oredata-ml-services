package com.oredata.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public final class DateTimeUtils {
    private static DateTimeUtils instance = null;
    private static final String EXPIRY_FORMAT = "MM.yyyy";
    public static final String DATE_FORMAT = "dd/MM/yyyy";
    public static final String DATE_FORMAT_FULL = "dd/MM/yyyy HH:mm:ss.SSS";
    public static final String STANDARD_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String DEFAULT_TIMEZONE = "Europe/Istanbul";
    private TimeZone localTZ = null;

    private DateTimeUtils() {
    }

    private DateTimeUtils(TimeZone timezone) {
        this.localTZ = timezone;
    }

    
    
    public Date getDateInLocalTimeZone(Date date){
        return getDateInTimezone(date, localTZ);
        
    }
    public synchronized static void init(String timezone) {
        TimeZone localTZ = timezone == null ? TimeZone.getDefault() : TimeZone.getTimeZone(timezone);
        instance = new DateTimeUtils(localTZ);
    }

    public synchronized static void initDefaultTimezone() {
        TimeZone localTZ = TimeZone.getTimeZone(DEFAULT_TIMEZONE);
        instance = new DateTimeUtils(localTZ);
    }

    /**
     * DateTimeUtils have to be inited first.
     * 
     * @return
     */
    public synchronized static DateTimeUtils getInstance() {
        if (instance == null) {
            initDefaultTimezone();
        }
        return instance;
    }

    /**
     * Returns UTC datetime from "yyyy-MM-dd HH:mm:ss.SSS" formatted datetime string according to localTZ TimeZone. Used
     * in DB insert / update statements.
     * 
     * @param datetime
     * @return
     */
    public Date getUTCDateTime(String datetime) {
        try {
            Date d = new SimpleDateFormat(STANDARD_FORMAT).parse(datetime);
            return new Date(d.getTime() - this.localTZ.getOffset(d.getTime()));
        } catch (ParseException e) {
            LoggerUtil.getInstance().getGeneralLogger().error("getUTCDateTime exception", e);
            return null;
        }
    }

    /**
     * Returns UTC datetime from formatted as df:DateFormat datetime string according to localTZ TimeZone. Used in DB
     * insert / update statements.
     * 
     * @param datetime
     * @param df
     * @return
     */
    public Date getUTCDateTime(String datetime, DateFormat df) {
        try {
            Date d = df.parse(datetime);
            return new Date(d.getTime() - this.localTZ.getOffset(d.getTime()));
        } catch (ParseException e) {
        }
        return null;
    }

    public boolean isValidDateStr(String datetime, DateFormat df) {
        boolean isValidDateStr = false;
        Date utcDateTime = getUTCDateTime(datetime, df);
        if (utcDateTime != null) {
            isValidDateStr = true;
        }
        return isValidDateStr;
    }

    /**
     * Returns now in UTC.
     * 
     * @return
     */
    public Date getUTCNowDateTime() {
        Date now = new Date();
        now.setTime(now.getTime() - this.localTZ.getOffset(now.getTime()));
        return now;
    }

    /**
     * Returns now in System Defined Timezone.
     * 
     * @return
     */
    public Date getLocalDate() {
        //FIXME  farkli timezone altinda bile calissa, tanimli olan TZ'daki date'i donmeli 
        return new Date();
    }

    /**
     * return local date time related timezone
     * 
     * @param timezone
     * @return
     */
    public Date getLocalDate(TimeZone timezone) {
        return new Date(getUTCNowDateTime().getTime() + timezone.getOffset(getUTCNowDateTime().getTime()));
    }

    /**
     * return date time related timezone
     * 
     * @param timezone
     * @return
     */
    public Date getDateInTimezone(Date date, TimeZone timezone) {
        return new Date(date.getTime() + timezone.getOffset(getUTCNowDateTime().getTime()));
    }

    /**
     * Returns UTC date.
     * 
     * @param c
     * @return
     */
    public Date getUTCDateTime(Calendar c) {
        return new Date(c.getTimeInMillis() - this.localTZ.getOffset(c.getTimeInMillis()));
    }

    public Date getUTCDateTime(Date inpDate) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(inpDate.getTime());
        return getUTCDateTime(c);
    }

    /**
     * Returns Local date according to the given UTC date.
     * 
     * @param utcDate
     * @return
     */
    public Date getLocalDateFromUTCDate(Date utcDate) {
        if (utcDate == null) {
            return null;
        }
        return new Date(utcDate.getTime() + this.localTZ.getOffset(utcDate.getTime()));
    }

    /**
     * @param dateTime
     * @param format
     * @return
     */
    public String getLocalDateFromUTCDate(Date dateTime, String format) {
        if (dateTime == null || format == null)
            return null;
        SimpleDateFormat formatTime = new SimpleDateFormat(format);
        formatTime.setTimeZone(this.localTZ);
        return (formatTime.format(getLocalDateFromUTCDate(dateTime)));
    }

    /**
     * Returns formatted string according to localTZ TimeZone. UTC to LOCAL. Used in DB read statements.
     * 
     * @param timestamp
     * @param format
     * @return
     */
    public String getFormattedDateFromUTC(Date dateTime, DateFormat format) {
        return format.format(new Date(dateTime.getTime() + this.localTZ.getOffset(dateTime.getTime())));
    }

    public Date getDateInTimeZoneFromUTCDate(Date dateTime, String timezone) {
        TimeZone localTZ = timezone == null ? TimeZone.getDefault() : TimeZone.getTimeZone(timezone);
        return new Date(dateTime.getTime() + localTZ.getOffset(dateTime.getTime()));
    }

    /**
     * Returns formatted string according to given TimeZone in standard format. UTC to given timezone. Used in DB read
     * statements.
     * 
     * @param dateTime
     * @param timezone
     * @return
     */
    public String getFormattedDateFromUTC(Date dateTime, TimeZone timezone) {
        if (dateTime == null)
            return "";
        return new SimpleDateFormat(STANDARD_FORMAT)
                .format(new Date(dateTime.getTime() + timezone.getOffset(dateTime.getTime())));
    }

    public String getFormattedLocalDateFromUTC(Date dateTime, TimeZone timezone, String format) {
        return new SimpleDateFormat(format)
                .format(new Date(dateTime.getTime() + timezone.getOffset(dateTime.getTime())));
    }

    public String getFormattedLocalDateFromUTC(Date dateTime, String timezone, String format) {
        TimeZone localTZ = timezone == null ? TimeZone.getDefault() : TimeZone.getTimeZone(timezone);
        return new SimpleDateFormat(format)
                .format(new Date(dateTime.getTime() + localTZ.getOffset(dateTime.getTime())));
    }

    /**
     * Returns formatted string.
     * 
     * @param dateTime
     * @param timezone
     * @return
     */
    public String getFormattedDate(Date dateTime, String format) {
        if (dateTime == null || format == null)
            return null;
        return new SimpleDateFormat(format).format(dateTime);
    }

    /**
     * returns date from format
     * 
     * @param date
     * @param format
     * @return
     * @throws ParseException
     */
    public Date getDateFromString(String date, String format) throws ParseException {
        if (date == null || format == null)
            return null;
        return new SimpleDateFormat(format).parse(date);
    }

    /**
     * Parse given string date with lenient option. If an exact format matching is needed, lenient should be given as
     * false.
     * 
     * @param date
     * @param format
     *            date format
     * @param lenient
     *            If an exact format matching is needed, lenient should be given as false.
     * @return
     * @throws ParseException
     */
    public Date getDateFromString(String date, String format, boolean lenient) throws ParseException {
        if (date == null || format == null)
            return null;
        SimpleDateFormat dateFormatter = new SimpleDateFormat(format);
        dateFormatter.setLenient(lenient);
        return dateFormatter.parse(date);
    }

    /**
     * Returns Standard DateFormat 'yyyy-MM-dd HH:mm:ss.SSS'
     * 
     * @return
     */
    public DateFormat getStandardFormat() {
        return new SimpleDateFormat(STANDARD_FORMAT);
    }

    /**
     * @param durationInDays
     * @return
     */
    public Date addDayToDate(int durationInDays) {
        Calendar rightNow = Calendar.getInstance();
        rightNow.add(Calendar.DAY_OF_MONTH, durationInDays);
        Date d2 = rightNow.getTime();
        return d2;
    }

    public Date addMillisecondsToDate(Long milliseconds) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(DateTimeUtils.getInstance().getUTCNowDateTime().getTime() + milliseconds);
        return cal.getTime();
    }

    public Date parseExpiry(String expiry) throws ParseException {
        DateFormat df = new SimpleDateFormat(EXPIRY_FORMAT);
        return df.parse(expiry);
    }

    /**
     * @param endDate
     * @param startDate
     * @param differenceType
     * @return
     */
    public long calculateDifference(Date endDate, Date startDate, int differenceType) {
        int direction = 1;
        if (startDate.after(endDate)) {
            direction = -1;
        }
        Calendar endDateCal = Calendar.getInstance();
        endDateCal.setTime(endDate);
        cleanCalendar(endDateCal, differenceType);
        Calendar startDateCal = Calendar.getInstance();
        startDateCal.setTime(startDate);
        cleanCalendar(startDateCal, differenceType);
        long difference = 0;
        while ((startDateCal.before(endDateCal) && direction == 1)
                || (startDateCal.after(endDateCal) && direction == -1)) {
            startDateCal.add(differenceType, direction);
            difference += direction;
        }
        return Math.abs(difference);
    }

    private void cleanCalendar(Calendar cal, int differenceType) {
        if (differenceType < Calendar.MILLISECOND) {
            cal.set(Calendar.MILLISECOND, 0);
        }
        if (differenceType < Calendar.SECOND) {
            cal.set(Calendar.SECOND, 0);
        }
        if (differenceType < Calendar.MINUTE) {
            cal.set(Calendar.MINUTE, 0);
        }
        if (differenceType < Calendar.HOUR_OF_DAY) {
            cal.set(Calendar.HOUR_OF_DAY, 0);
        }
        if (differenceType < Calendar.DATE) {
            cal.set(Calendar.DATE, 1);
        }
        if (differenceType < Calendar.MONTH) {
            cal.set(Calendar.MONTH, 1);
        }
        if (differenceType < Calendar.YEAR) {
            cal.set(Calendar.YEAR, 0);
        }
    }

    public Date addDifference(Date date, int differenceType, int difference) {
        Calendar startDateCal = Calendar.getInstance();
        startDateCal.setTime(date);
        startDateCal.add(differenceType, difference);
        return startDateCal.getTime();
    }

    public java.sql.Date getSqlDate(Date d) {
        return new java.sql.Date(d.getTime());
    }

    public boolean isFutureUTCDate(Date utcDate) {
        boolean isFutureUTCDate = false;
        if (utcDate != null) {
            if (utcDate.compareTo(getUTCNowDateTime()) > 0) {
                isFutureUTCDate = true;
            }
        }
        return isFutureUTCDate;
    }

}
