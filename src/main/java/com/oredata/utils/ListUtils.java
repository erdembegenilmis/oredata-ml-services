package com.oredata.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

public class ListUtils {

    public static List<String> getList(String onlyElement) {
        List<String> list = new ArrayList<String>(1);
        list.add(onlyElement);
        return list;
    }

    public static <T> List<T> getList4UncertainNumberOfParams(T... elements) {
        List<T> list = new ArrayList<T>();
        for (T element : elements) {
            if (element != null) {
                list.add(element);
            }
        }
        return list;
    }

    public static List<String> getList(String[] elements) {
        if (elements == null) {
            return null;
        }
        List<String> list = new ArrayList<String>(elements.length);
        for (int i = 0; i < elements.length; i++)
            list.add(elements[i]);
        return list;
    }

    public static List<String> getList(String elements, String regex) {
        if (elements == null) {
            return null;
        }
        return ListUtils.getList(elements.split(regex));
    }

    public static String getFirstString(List<String> list) {
        if (list == null)
            return null;
        for (String s : list)
            return s;
        return null;
    }

    public static String toString(List<String> list) {
        if (list != null) {
            if (list.size() == 1)
                return list.get(0);
            StringBuffer b = new StringBuffer();
            b.append("List [" + list.toString() + "]");
            // b.append(" {");
            // for (String string : list)
            //	b.append("[" + string + "]");
            // b.append("}");
            return b.toString();
        }
        return "List is null.";
    }

    public static String[] getSplittedArray(String arrayStr, String splitParam) {
        if (StringUtils.isEmpty(arrayStr)) {
            return new String[0];
        } else {
            return arrayStr.split(splitParam);
        }

    }

    public static void main(String[] args) {
        List<String> s = ListUtils.getList("SALE|PRE|POST|VOID|CRED|RFND", "\\|");
        System.out.println(s.size() + " List: " + s.toString());
    }

    /**
     * @param elements
     * @param regex
     * @return longList
     */
    public static List<Long> getLongList(String elements, String regex) {
        List<String> listStr = getList(elements, regex);
        List<Long> list = new ArrayList<Long>();
        for (String string : listStr) {
            list.add(Long.parseLong(string));
        }
        return list;
    }

    /**
     * Verilen büyük boyutlu listeyi size ile belirtilen adetlere göre küçük parçalara böler. sqlde IN clause için
     * kullanımı uygundur.
     * 
     * @param collection
     * @return
     */
    public static List<List<String>> splitListToSublist(List<String> inputList, int size) {
        List<List<String>> retList = new ArrayList<List<String>>();
        int part = (inputList.size() / size);
        int left = (inputList.size() % size);
        if (left != 0)
            part++;
        for (int i = 0; i < part; i++) {
            if (i != part - 1)
                retList.add(inputList.subList(i * size, (i + 1) * size));
            else
                retList.add(inputList.subList(i * size, inputList.size()));
        }
        return retList;
    }

    public static boolean isNullOrEmptyList(List<?> inputList) {
        if (inputList == null)
            return true;
        if (inputList.isEmpty())
            return true;
        return false;
    }

    public static <T> void printAllList(Collection<T> inputList) {
        for (T element : inputList) {
            System.out.println(element);
        }
    }

    public static <K, V> void addMultiValue2Map(Map<K, List<V>> multiValueMap, K key, V value) {
        List<V> multiValueList = multiValueMap.get(key);
        if (multiValueList == null) {
            multiValueList = new ArrayList<>();
            multiValueList.add(value);
            multiValueMap.put(key, multiValueList);
        } else {
            multiValueList.add(value);
        }
    }

}
