package com.oredata.listeners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.oredata.nlp.sentiment.SentimentAnalyzer;

@WebListener
public class SentimentContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        SentimentAnalyzer.GenerateRegressionVectors();
        SentimentAnalyzer.TrainRFRegressorModel();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }

}
