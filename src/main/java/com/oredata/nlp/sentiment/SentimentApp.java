package com.oredata.nlp.sentiment;

public class SentimentApp {
    public static void main(String[] args) {
        //        SentimentAnalyzer.SaveVectorRepresentations();
        SentimentAnalyzer.GenerateRegressionVectors();
        SentimentAnalyzer.TrainRFRegressorModel();
//        System.out.println(SentimentAnalyzer.GetNewsSentimentScore("Title",
//                "Türkiye İstatistik Kurumu (TÜİK), 2016 yılı Şubat ayına ilişkin konut satış istatistiklerini açıkladı."
//                        + "Buna göre, Türkiye genelinde konut satışları 2017 Şubat ayında bir önceki yılın aynı ayına göre "
//                        + "yüzde 0,2 oranında azalarak 101 bin 468 oldu. Konut satışlarında, İstanbul 17 bin 783 konut"
//                        + " satışı ve yüzde 17,5 ile en yüksek paya sahip oldu. Satış sayılarına göre İstanbul'u, 11 bin "
//                        + "274 konut satışı ve yüzde 11,1 pay ile Ankara, 6 bin 290 konut satışı ve yüzde 6,2 pay"
//                        + " ile İzmir izledi. Konut satış sayısının düşük olduğu iller sırasıyla 4 konut ile Hakkari, "
//                        + "7 konut ile Ardahan ve 27 konut ile Şırnak oldu."));
//
//        System.out.println("Sozcu Haber" + SentimentAnalyzer.GetNewsSentimentScore("Title",
//                "OSD açıklamasında, “Toplam üretim, Ocak-Şubat dönemi üretimleri tarihsel gelişimi içinde sanayimizin en yüksek seviyesine ulaşmış oldu” denildi."
//                        + "Buna göre, Ocak-Şubat döneminde, otomobil üretimi geçen yılın aynı dönemine göre yüzde 46 artışla 191 bin adete çıkarken, otomobil satışları yüzde 6.0 daralmayla ve 60 bin adete geriledi. Bu dönemde ithal otomobil satışları yüzde 12 azalırken, yerli otomobil satışları yüzde 12 arttı."
//                        + "Aynı dönemde, otomotiv sanayi toplam üretimi de yüzde 22 yükselişle 266 bin adet düzeyine çıkarken, toplam satışlar yüzde 4.0 daralarak 84 bin adet düzeyine düştü."
//                        + "Ticari araç grubunda, 2017 yılı Ocak-Şubat döneminde üretim yüzde 13 ve hafif ticari araç grubunda yüzde 16 azalırken, ağır ticari araç grubunda ise yüzde 49 düzeyinde arttı."
//                        + "OSD açıklamasında, “Ağır ticarideki bu değişim, baz etkisi kaynaklı olup 2015 yılı Ocak-Şubat dönemine göre üretim yüzde 40 azaldı. Aynı dönemde ticari araç pazarı ise geçen yılın aynı dönemine paralel sonuçlandı. Pazar, hafif ticari araç grubunda yüzde 2 artarken, ağır ticari araç grubunda ise yüzde 17 geriledi” denildi."
//                        + "Ocak-Şubat döneminde bir önceki yılın aynı dönemine göre, toplam otomotiv ihracatı adet bazında yüzde 31 artarken otomobil ihracatı ise yüzde 60 artış gösterdi. Bu dönemde, toplam ihracat 219 bin adet, otomobil ihracatı ise 158 bin adet düzeyinde gerçekleşti."
//                        + "* Aynı dönemler itibarıyla, toplam otomotiv ihracatı dolar bazında yüzde 21, euro bazında yüzde 25 arttı."
//                        + "* Bu dönemde toplam otomotiv ihracatı 4.33 milyar dolara çıkarken, otomobil ihracatı yüzde 92 artarak 1.84 milyar dolara yükseldi. Euro bazında otomobil ihracatı ise yüzde 99 artarak 1.73 milyar euroya çıktı."
//                        + "* Ocak-Şubat döneminde geçen yıla göre toplam hafif ticari araç satışları yüzde 2.0 ve ithal hafif ticari araç satışları yüzde 8.0 artarken, yerli hafif ticari araç satışları yüzde 4.0 azaldı."
//                        + "* Bu dönemde; geçen yıla göre ağır ticari araç pazarı yüzde 17 azalarak 2 bin adet, kamyon pazarı yüzde 16 azalarak 1,573 adet, midibüs pazarı yüzde 21 azalarak 269 adet ve otobüs pazarı ise yüzde 25 azalarak 159 adet düzeyinde gerçekleşti."
//                        + "* Son 10 yıllık ortalamalara göre 2017 yılı Ocak-Şubat döneminde toplam pazar yüzde 12 ve otomobil pazarı yüzde 22 artarken, ağır ticari araç pazarı yüzde 50 düşüş gösterdi. Hafif ticari araç pazarı son 10 yıllık ortalamaya paralel sonuçlandı."
//                        + "* 2017 yılı Ocak-Şubat döneminde 158 bin adedi otomobil olmak üzere, toplam üretimin yüzde 82’sini oluşturan 219 bin adet taşıt ihraç edildi. 2017 yılı Ocak-Şubat döneminde gerçekleşen taşıt aracı ihracatı, 2016 yılına göre yüzde 31 arttı."
//                        + "* Bu dönemde otomobil ihracatı bir önceki yılın aynı ayına göre yüzde 60 artarken,  ticari araç ihracatı yüzde 11 azaldı. Traktör ihracatı ise 2016 yılı Ocak-Şubat dönemine göre yüzde 24 azalarak bin 874 adet olarak gerçekleşti."
//                        + "* Uludağ İhracatçı Birlikleri (UİB) verilerine göre, Ocak-Şubat döneminde toplam ihracat, 2016 yılı aynı dönemine göre yüzde 21 arttı ve 4 milyar 334 milyon dolar oldu. Euro bazında ise yüzde 25 artarak 4.1 milyar euro olarak gerçekleşti."
//                        + "* Bu dönemde, ana sanayi ihracatı yüzde 32, yan sanayi ihracatı yüzde 4.0 arttı."
//                        + "* 2017 yılı Ocak-Şubat döneminde traktör üretimi ile birlikte toplam üretim 273 bin 978 adet olarak gerçekleşti."
//                        + "* 2016 yılı Ocak-Şubat dönemine göre, yük ve yolcu taşıyan ticari araçlar üretimi, 2017 yılı Ocak-Şubat döneminde toplamda yüzde 13 azalırken, ürün grubu bazında:"
//                        + "– K. Kamyonda yüzde 648" + "– Minibüste yüzde 165" + "– B. Kamyonda yüzde 58"
//                        + "– Midibüste yüzde 43" + "– Otobüste yüzde 14 artarken," + "– Kamyonette yüzde  22 azaldı."));
//        
//        
//        System.out.println("Euro Bölgesi'nde perakende satışlar azaldı : " + SentimentAnalyzer.GetNewsSentimentScore("Title",
//                "Avrupa İstatistik Ofisinin (Eurostat) bugün açıkladığı verilere göre, bu yılın ocak ayında mevsimsellikten arındırılmış perakende satışlar bir önceki aya kıyasla 19 üyeli Euro Bölgesi'nde yüzde 0,1 azaldı. Perakende satışlar, 2016'nın ocak ayına göre ise yüzde 1,2 yükseldi."
//                        +"Veriler ayrıca, 28 üyeli AB'de perakende satışların ocakta aylık bazda yüzde 0,1 ve yıllık bazda da yüzde 1,5 yükseldiğini ortaya koydu."
//                        +"Verinin mevcut olduğu ülkeler arasında, ocakta aylık bazda perakende satışında azalış görülen ülkeler sırasıyla yüzde 0,8'le Almanya ile Fransa, yüzde 0,6 ile Belçika ve yüzde 0,4'le de Birleşik Krallık olurken, en fazla artış görülen ülke ise yüzde 4,4 ile Finlandiya oldu. Bunu yüzde 4,1'le Polonya ve yüzde 3,2 ile de Slovenya takip etti."
//                        +"Yılık bazda kıyaslandığında ise ocak ayında perakende satışlarında en fazla artış görülen ülke yüzde 15,4'le Slovenya oldu. Ardından yüzde 8,8'le Lüksemburg, yüzde 8,5'le de Litvanya geldi."
//                        +"Söz konusu dönemde perakende satışlarında azalış görülen tek üye ülke ise yüzde 0,4'le Almanya olarak belirlendi."
//
//));
        
        System.out.println("Kebapçı İflas " + SentimentAnalyzer.getNewsSentimentScore("Title",
                "Hacıbey Kebapçılık Gıda San. Tic. şirketi için geçtiğimiz Haziran ayı başında iflas erteleme başvurusunda bulunulmuştu. Dava başvurusu İstanbul Anadolu 8.Son dakika: 67 yıllık ünlü kebapçı iflas etti Asliye Ticaret Mahkemesi’ne yapıldı. Şirket adına yapılan iflas erteleme başvurusunda, şirketin borca batık olduğu kaydedildi. Şirket hakkında erteleme verilmesi durumunda, faaliyetlerin yeniden etkin bir biçimde sürdürülebileceği kaydedilmişti."
                        +"MAHKEME KAYYUM ATADI"
                        +"Mahkeme, dosya üzerinde tedbir kararı verdi. Şirkete kayyum ataması yapılmıştı. Şirket hakkında tedbir kararı verilirken yargılamaya da devam edildi. Ancak yapılan yargılama sonrası mahkeme, şirketin kurtarılamayacağına karar verdi. Mahkeme şirket hakkında iflas kararı vererek dosyayı, tasfiye işlemleri için icra iflas müdürlüğüne gönderdi."
                        +"67 YIL ÖNCE TEMELİ ATILDI"
                        +"Bursa merkezli Hacıbey Döner Lokantaları sektörün köklü firmalarından biri. Hacıbey markası Hüseyin Hacıbeyoğlu tarafından 1950 yılında yaratıldı. Bursa'nın bu köklü markası 1992 yılında da İstanbul'a adım attı. Aynı yıl Kadıköy, 1994'te İse Teşvikiye'de şubeler açtı. Daha sonraki yıllarda Teşvikiye'deki mekân kapandı."
                        +"Son dakika: 67 yıllık ünlü kebapçı iflas etti"
                        +"YENİ MEKÂN SIKINTIYA NEDEN OLDU"
                        +"Şirket, bu yılın başında ise İstanbul Ataşehir’de iddialı bir mekân açmıştı. Hüseyin Hacıbeyoğlu’nun kurucusu olduğu markanın yönetiminde şimdilerde İsmet Hacıbeyoğlu bulunuyor. Şirketin mali sıkıntıya girmesine, söz konusu mekânın açılışı ile ilgili yaşanan sorunların neden olduğu belirtilmişti."
                        +"Son dakika: 67 yıllık ünlü kebapçı iflas etti"
                        +"RAPORLAR DA KÖTÜ GELDİ"
                        +"Şirketin avukatı Serkan Duran, gelen bilirkişi raporları doğrultusunda mahkemenin iflas kararı verdiğini belirtti. Av. Duran, kararı temyiz ettiklerini. Kararın kesinleşmesi ile birlikte tasfiye sürecine geçileceğini kaydetti. "


));

    }

}
