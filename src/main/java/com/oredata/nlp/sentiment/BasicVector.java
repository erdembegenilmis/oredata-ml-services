package com.oredata.nlp.sentiment;

public class BasicVector {
    public static double[] AddTwoVector(double[] arg1, double[] arg2) {
        double[] retVal = new double[arg1.length];
        int arrayLength = arg1.length;
        
        for(int elemCounter = 0 ; elemCounter < arrayLength ; elemCounter++) {
            retVal[elemCounter] = arg1[elemCounter] + arg2[elemCounter];
        }
        
        return retVal;      
    }

}
