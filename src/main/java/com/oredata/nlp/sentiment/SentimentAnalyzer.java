package com.oredata.nlp.sentiment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.ml.Pipeline;
import org.apache.spark.ml.PipelineModel;
import org.apache.spark.ml.PipelineStage;
import org.apache.spark.ml.feature.LabeledPoint;
import org.apache.spark.ml.feature.VectorIndexer;
import org.apache.spark.ml.feature.VectorIndexerModel;
import org.apache.spark.ml.linalg.Vectors;
import org.apache.spark.ml.regression.RandomForestRegressor;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.text.sentenceiterator.FileSentenceIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;

public class SentimentAnalyzer {

    private static PipelineModel model = null;
    private static WordVectors wordVector = null;

    private static double SUBSAMLING_RATE = 1e-5;
    private static int MIN_WORD_FREQUENCY = 5;
    private static int BATCH_SIZE = 1000;
    private static int HIDDEN_LAYER_SIZE = 100;
    private static int ITERATION_COUNT = 20;
    private static double INITIAL_LEARNING_RATE = 0.025;
    private static double MIN_LEARNING_RATE = 1e-2;
    private static double NEGATIVE_SAMPLING_RATE = 10;
    private static ArrayList<String> stopWordsList = new ArrayList<String>(Arrays.asList("\""));

    /* input files */
    private static String FREE_TEXT_FILE_NAME = "/Users/velioglub/Desktop/Workspace/Oredata/lastPart.txt";
    private static String LABELED_DATA_FILE_NAME = "/data/Oredata/app/odeabank/sentimentFiles/labeledData.txt";

    /* output files*/
    private static String REPRESENTATION_FILE_NAME = "/data/Oredata/app/odeabank/sentimentFiles/representations.txt";
    private static String SPARK_FORMATTED_FILE_NAME = "/data/Oredata/app/odeabank/sentimentFiles/sparkRegressionFormat.txt";

    private static SparkSession sparkSession;
    private static SparkContext sc;
    private static JavaSparkContext jsc;
    private static SQLContext sqlContext;

    /* 
     * SaveVectorRepresentations gets the file with sentences (format is a sentence per line)
     * and generates vector representation for each word. Then, save these representations to
     * the outputFileName.
     */
    public static void SaveVectorRepresentations() {

        File file = new File(FREE_TEXT_FILE_NAME);
        SentenceIterator iter = new FileSentenceIterator(file);
        TokenizerFactory t = new DefaultTokenizerFactory();

        Word2Vec w2vModel = new Word2Vec.Builder().sampling(SUBSAMLING_RATE).minWordFrequency(MIN_WORD_FREQUENCY)
                .batchSize(BATCH_SIZE).layerSize(HIDDEN_LAYER_SIZE).iterations(ITERATION_COUNT)
                .learningRate(INITIAL_LEARNING_RATE).minLearningRate(MIN_LEARNING_RATE)
                .negativeSample(NEGATIVE_SAMPLING_RATE).useAdaGrad(false).stopWords(stopWordsList).iterate(iter)
                .tokenizerFactory(t).build();

        try {
            w2vModel.fit();
            WordVectorSerializer.writeWordVectors(w2vModel, REPRESENTATION_FILE_NAME);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
     * GenerateRegressionVectors generated spark formatted regression vector for the given
     * labeled data file. Save the representation to outputFileName. 
     */
    public static void GenerateRegressionVectors() {

        File labeledDataFile = new File(LABELED_DATA_FILE_NAME);

        BufferedReader bufferedReader = null;
        FileReader fileReader = null;
        PrintWriter writer = null;

        try {
            if (wordVector == null) {
                File representationFile = new File(REPRESENTATION_FILE_NAME);
                wordVector = WordVectorSerializer.loadTxtVectors(representationFile);
            }

            fileReader = new FileReader(labeledDataFile);
            bufferedReader = new BufferedReader(fileReader);
            String line = null;

            writer = new PrintWriter(SPARK_FORMATTED_FILE_NAME, "UTF-8");

            while ((line = bufferedReader.readLine()) != null) {

                String[] curPartsOfLine = line.split(",");

                // According to file format
                String curSentence = curPartsOfLine[1].replace("\"", "");
                double sentenceScore = Double.parseDouble(curPartsOfLine[curPartsOfLine.length - 1].replace("\"", ""));

                double[] curRepresentation = GetVectorRepresentation(wordVector, curSentence);
                WriteRepresentationInSparkFormat(writer, curRepresentation, sentenceScore);
            }
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
     * GetVectorRepresentation converts sentence to vector representation. You should use same 
     * HIDDEN_LAYER_SIZE with SaveVectorRepresentations.
     */
    private static double[] GetVectorRepresentation(WordVectors wordVecs, String sentence) {
        double[] curRepresentation = new double[HIDDEN_LAYER_SIZE];
        String[] curWordList = sentence.split("\\s+");

        /* although it is unnecessary, it makes the code more readable */
        Arrays.fill(curRepresentation, 0);

        for (String word : curWordList) {
            curRepresentation = BasicVector.AddTwoVector(curRepresentation, wordVecs.getWordVector(word));
        }

        return curRepresentation;
    }

    /*
     * You can refer to https://github.com/apache/spark/blob/master/data/mllib/sample_linear_regression_data.txt
     * to understand the format used by spark to load regression data.
     */
    private static void WriteRepresentationInSparkFormat(PrintWriter writer, double[] vectorRep, double score) {

        int featureCounter = 0;

        writer.print(score);

        for (; featureCounter < vectorRep.length; featureCounter++) {
            writer.print(" " + (featureCounter + 1) + ":" + vectorRep[featureCounter]);
        }

        writer.println();
    }

    /*
     * TrainRFRegressorModel train a RF regressor model using the labeled data formatted as
     * spark uses. You can generate the formatted file using GenerateRegressionVectors function.
     */
    public static void TrainRFRegressorModel() {
        sparkSession = SparkSession.builder().master("local").getOrCreate();
        sc = sparkSession.sparkContext();
        jsc = new JavaSparkContext(sc);
        sqlContext = new SQLContext(sparkSession);

        Dataset<Row> data = sparkSession.read().format("libsvm").load(SPARK_FORMATTED_FILE_NAME);

        VectorIndexerModel featureIndexer = new VectorIndexer().setInputCol("features").setOutputCol("indexedFeatures")
                .fit(data);

        // Train a RandomForest model.
        RandomForestRegressor rf = new RandomForestRegressor().setLabelCol("label").setFeaturesCol("indexedFeatures");

        // Chain indexer and forest in a Pipeline
        Pipeline pipeline = new Pipeline().setStages(new PipelineStage[] { featureIndexer, rf });

        // Train model and save it. This also runs the indexer.
        model = pipeline.fit(data);
    }

    public static double getNewsSentimentScore(String title, String content) {
        // init word vector
        if (wordVector == null) {
            try {
                File representationFile = new File(REPRESENTATION_FILE_NAME);
                wordVector = WordVectorSerializer.loadTxtVectors(representationFile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        // prepare test data
        // TODO : Absolutely need for polishing !!!
        double[] contentRepresentation = GetVectorRepresentation(wordVector, content);
        LabeledPoint pos = new LabeledPoint(0d, Vectors.dense(contentRepresentation));
        List<LabeledPoint> testDataList = new ArrayList<>();
        testDataList.add(pos);
        JavaRDD<LabeledPoint> labeledDataRDD = jsc.parallelize(testDataList);
        Dataset<Row> testData = sqlContext.createDataFrame(labeledDataRDD, LabeledPoint.class);
        // do regression
        Dataset<Row> predictions = model.transform(testData);
        double regressionScore = predictions.first().getDouble(3);
        return regressionScore;
    }
}
