package com.oredata.services;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.oredata.nlp.sentiment.SentimentAnalyzer;
import com.oredata.utils.LoggerUtil;

@Path("/mlServices")
public class MachineLearningServices {

    @POST
    @Path("/getNewsSentimentScore")
    @Produces(MediaType.APPLICATION_JSON)
    public String getNewsSentimentScore(@Context HttpHeaders headers, String bodyRequest) {
        try {

            JsonElement jelement = new JsonParser().parse(bodyRequest);
            JsonObject jobject = jelement.getAsJsonObject();
            String content = jobject.get("content").toString();
            System.out.println("Sentiment Content : " + content);
            double newsSentimentScore = SentimentAnalyzer.getNewsSentimentScore("", content);
            System.out.println("Sentiment Score is : " + newsSentimentScore);
            jobject.addProperty("sentimentScore", newsSentimentScore);

            String newsObjectStr = jobject.toString();
            System.out.println("News Sentimented Json : " + newsObjectStr);
            return newsObjectStr;
        } catch (Exception e) {
            e.printStackTrace();
            LoggerUtil.getInstance().getGeneralLogger().error("Error in getNewsSentimentScore.", e);
            return null;
        }
    }
    
    public static void main(String[] args) {
        String bodyRequest = "{\"content\":\"kjhsdkjsdl\"}";
        JsonElement jelement = new JsonParser().parse(bodyRequest);
        JsonObject jobject = jelement.getAsJsonObject();
        String content = jobject.get("content").toString();
        System.out.println("Sentiment Content : " + content);
        double newsSentimentScore = 0.3d;
        System.out.println("Sentiment Score is : " + newsSentimentScore);
        jobject.addProperty("sentimentScore", newsSentimentScore);

        String newsObjectStr = jobject.toString();
        System.out.println("News Sentimented Json : " + newsObjectStr);
    }

}