package com.oredata.news.sentiment.services;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.apache.commons.io.FileUtils;

public class DuplicateLineRemover {

	public static void main(String[] args) {
		HashSet<String> uniqueSentences = new HashSet<String>();
		StringBuffer uniqueSentencesBuffer = new StringBuffer();
		try {
			String sentences = FileUtils.readFileToString(new File(App.STEM_TXT));
			StringTokenizer lineSeperator = new StringTokenizer(sentences, "\n");
			while(lineSeperator.hasMoreTokens()){
				String sentence = lineSeperator.nextToken();
				if(!uniqueSentences.contains(sentence)){
					uniqueSentences.add(sentence);
					uniqueSentencesBuffer.append(sentence).append("\n");
				}
			}
			FileUtils.writeStringToFile(new File(App.UNIQUE_SENTENCE_FILE), uniqueSentencesBuffer.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
