package com.oredata.news.sentiment.services;

import zemberek.morphology.analysis.WordAnalysis;
import zemberek.morphology.analysis.tr.TurkishMorphology;
import zemberek.morphology.lexicon.Suffix;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class ConvertToStem {
	
    public static String convertToStem(TurkishMorphology morphology, String sentence) throws IOException {
    	
    	// System.out.println("Sentence is : " + sentence);
    	String[] words = sentence.split(" ");
    	StringBuilder sb = new StringBuilder();
        for(String word : words) {
        	// System.out.println("Word is : " + word);
        	if (word != null && word.length() != 0 && !StringUtils.isNumeric(word)) {
        		// System.out.println("Second word is : " + word);
        		if (morphology == null)
        			System.out.println("Morphology is null");
		        List<WordAnalysis> results = morphology.analyze(word);      
		        
		        if(results.size() <= 0)
		        	return null;
		        
		        WordAnalysis result = results.get(0);
		        
		        // Get the root of the word
		    	String root =  result.root;
		    	
		    	for (int i = 0 ; i < result.getSuffixDataList().size() ; i++) {
		    		Suffix sff = result.getSuffixes().get(i);
		    		
		    		if (sff.toString().equalsIgnoreCase("Neg")) {
		    			root = root + result.getSuffixDataList().get(i).lex;
		    		}
		    	}
		    	
		    	sb.append(root.toLowerCase() + " ");
        	}
        }
        
        return sb.toString();
        
    }
}