package com.oredata.news.sentiment.services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.text.sentenceiterator.BasicLineIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import zemberek.morphology.analysis.tr.TurkishMorphology;

import com.oredata.news.sentiment.models.SentenceVector;

public class App {

    private static Logger log = LoggerFactory.getLogger("com.oredata.news.sentiment.services.App");
    private static Word2Vec vec;
    
    private static int VECTOR_LENGTH = 25;
    
    public static String STEM_TXT = "word2Vec.log";
    public static String UNIQUE_SENTENCE_FILE = "unique.txt";
    private static String CSV_FILE_NAME = "/Users/omerfarukkurt/Downloads/semanticTrainingSet.log";
    private static String OUT_FILE_NAME = "out.txt";
    
    private static void generateWordVectors() throws IOException {
        log.info("Load & Vectorize Sentences....");
        // Strip white space before and after for each line
        SentenceIterator iter = new BasicLineIterator(UNIQUE_SENTENCE_FILE);
        // Split on white spaces in the line to get words
        TokenizerFactory t = new DefaultTokenizerFactory();

        // Tokenize the sentence
        t.setTokenPreProcessor(new CommonPreprocessor());

        log.info("Building model....");
        vec = new Word2Vec.Builder()
                .minWordFrequency(5)
                .iterations(1) // Increase the number to increase over-fit level.
                .layerSize(VECTOR_LENGTH) // Increases the dimension of the latent representation
                .seed(42)
                .windowSize(5) // Window size to create vectors
                .iterate(iter)
                .tokenizerFactory(t)
                .build();

        log.info("Fitting Word2Vec model....");
        vec.fit();
    }

    @SuppressWarnings("resource")
	private static ArrayList<SentenceVector> getSentenceVectors(TurkishMorphology morphology, String fileName) throws IOException {
    	ArrayList<SentenceVector> retList = new ArrayList<SentenceVector>();
    	
    	BufferedReader br = null;
		String sCurrentLine;
		
		br = new BufferedReader(new FileReader(fileName));

		while ((sCurrentLine = br.readLine()) != null) {
			
			// Replace chars 
			sCurrentLine = sCurrentLine.replaceAll("\"", "");
			sCurrentLine = sCurrentLine.replaceAll("'", "");
			
			String[] country = sCurrentLine.split(",");
			
			SentenceVector sv = new SentenceVector(country[1].replace("\"", "").replace("'", ""), VECTOR_LENGTH);
			sv.generateSentenceVector(morphology, vec);
			
			retList.add(sv);
		}
		
		return retList;    	
    }
    
    private static void saveSentencesToFile(ArrayList<SentenceVector> sv) throws IOException {
    	FileWriter fw = new FileWriter(OUT_FILE_NAME);
    	
		BufferedWriter bw = new BufferedWriter(fw);
    	
    	for(SentenceVector curSentence : sv) {
    		bw.write(curSentence.toString() + "\n");
    	}
    	
    	bw.close();
    }
    
    public static void main(String[] args) throws Exception {
    	
    	// Run Word2Vec to obtain vectors for each unique word stem
    	generateWordVectors();
    	
    	// Morphology to form sentence vectors
    	TurkishMorphology morphology = TurkishMorphology.createWithDefaults();    	
    	ArrayList<SentenceVector> sv = getSentenceVectors(morphology, CSV_FILE_NAME);
    	
    	saveSentencesToFile(sv);   	
    }
}