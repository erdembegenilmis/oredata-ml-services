package com.oredata.news.sentiment.zemberek;

import zemberek.morphology.analysis.WordAnalysis;
import zemberek.morphology.analysis.tr.TurkishMorphology;
import zemberek.morphology.lexicon.Suffix;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class ConvertToStem {
	
    TurkishMorphology morphology;
    private static final String OUT_FILENAME = "stem_sent.txt";
    private static String FILENAME = "/Users/oredata_ds/Desktop/Oredata/Odeabank/Data/allDoc.txt";
    private static BufferedWriter bw = null;
	private static FileWriter fw = null;
    
    public ConvertToStem(TurkishMorphology morphology) {
        this.morphology = morphology;
    }
    
    public void analyze(String sentence) throws IOException {

    	String[] words = sentence.split(" ");
    	StringBuilder sb = new StringBuilder();
        for(String word : words) {
	        List<WordAnalysis> results = morphology.analyze(word);      
	        
	        if(results.size() <= 0)
	        	return;
	        
	        WordAnalysis result = results.get(0);
	        
	        // Get the root of the word
	    	String root =  result.root;
	    	
	    	for (int i = 0 ; i < result.getSuffixDataList().size() ; i++) {
	    		Suffix sff = result.getSuffixes().get(i);
	    		
	    		if (sff.toString().equalsIgnoreCase("Neg")) {
	    			root = root + result.getSuffixDataList().get(i).lex;
	    		}
	    	}
	    	
	    	sb.append(root.toLowerCase() + " ");
        }
        
        bw.write(sb.toString() + "\n");
        
    }
 

    public static void main(String[] args) throws IOException {
    	
    	TurkishMorphology morphology = TurkishMorphology.createWithDefaults();
    	
    	BufferedReader br = null;
		FileReader fr = null;		

		try {
			
			fw = new FileWriter(OUT_FILENAME);
			bw = new BufferedWriter(fw);

			fr = new FileReader(FILENAME);
			br = new BufferedReader(fr);

			String sCurrentLine;
			
			br = new BufferedReader(new FileReader(FILENAME));

			while ((sCurrentLine = br.readLine()) != null) {
				
				// Replace chars 
				sCurrentLine = sCurrentLine.replaceAll("\"", "");
				sCurrentLine = sCurrentLine.replaceAll("'", "");
				
				
				System.out.println(sCurrentLine);
				new ConvertToStem(morphology).analyze(sCurrentLine);
			}

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (br != null)
					br.close();

				if (fr != null)
					fr.close();
				
				if (bw != null)
					bw.close();

				if (fw != null)
					fw.close();
				
			} catch (IOException ex) {

				ex.printStackTrace();

			}
		}    	
    }
}