package com.oredata.news.sentiment.models;

import java.io.IOException;

import org.deeplearning4j.models.word2vec.Word2Vec;

import com.oredata.news.sentiment.services.ConvertToStem;

import zemberek.morphology.analysis.tr.TurkishMorphology;

public class SentenceVector {
	private String sentence;
	private double[] sentenceVector;
	
	public SentenceVector(String sentence, int vectorLength) {
		this.sentence = sentence;
		sentenceVector = new double[vectorLength];
	}
	
	public void generateSentenceVector(TurkishMorphology morphology, Word2Vec vec) throws IOException{
//		String sen2Split = ConvertToStem.convertToStem(morphology, sentence).trim();
		String sen2Split = sentence.trim();
		
		String[] words = sen2Split.split(" ");
		
		try {
			System.out.println(words[0] + " --> ");
			sentenceVector = vec.getWordVector(words[0]);
			System.out.println(sentenceVector);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		for(int i = 1 ; i < words.length ; i++) {
			try {
				System.out.println(words[i] + " --> ");
				double[] wordVector = vec.getWordVector(words[0]);
				System.out.println(wordVector);
				sentenceVector = addVectors(sentenceVector, wordVector);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private double[] addVectors(double[] vec1, double[] vec2) {
		if(vec1 == null){
			return vec2;
		}
		double []retVec = vec1;
		
		for(int i = 0 ; i < vec1.length ; i++) {
			retVec[i] += vec2[i];
		}
		
		return retVec;
	}

	public String getSentence() {
		return sentence;
	}

	public double[] getSenVec() {
		return sentenceVector;
	}

	@Override
	public String toString() {
		StringBuilder resString = new StringBuilder();
		
		// Add the sentence
		resString.append(getSentence() + ',');
		
		for(double curDim : getSenVec()) {
			resString.append(curDim);
			resString.append(',');
		}
		
		return resString.toString().substring(0, resString.length() - 1);
	}
	
	
}
