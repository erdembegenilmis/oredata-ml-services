package com.oredata.news.sentiment.word2vec;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.deeplearning4j.text.sentenceiterator.LineSentenceIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.sentenceiterator.SentencePreProcessor;

import visad.CommonUnit;
import zemberek.morphology.analysis.WordAnalysis;
import zemberek.morphology.analysis.tr.TurkishMorphology;
import zemberek.morphology.lexicon.Suffix;

public class Word2VecFileGenerator {
	private static Logger logger = Logger.getLogger("com.oredata.news.sentiment.word2vec");

	public static void generateSentence(String text){
		logger.info(text);
	}
	
	public static boolean isNumeric(String text){
		return text.matches("^[-+]?\\d+(\\,\\d+)?$");
	}
	
	public static boolean hasCharacter(String text){
		return text.matches(".*[a-zA-Z]+.*");
	}
	
	public static void main(String[] args) {
		try {
			
//			SentenceIterator iter = UimaSentenceIterator.create("path/to/your/text/documents");
			
			String text = FileUtils.readFileToString(new File("word2Vec.log"));
			
			
			
			TurkishMorphology morphology = TurkishMorphology.createWithDefaults();
			
			//
			
			List<WordAnalysis> wordAnalysisList = morphology.analyze("Bugünlerde");
			
			for (Iterator iterator = wordAnalysisList.iterator(); iterator.hasNext();) {
				WordAnalysis wordAnalysis = (WordAnalysis) iterator.next();
				System.out.println("Lemma --> " + wordAnalysis.getLemma());
				List<String> stems = wordAnalysis.getStems();
				for (Iterator iterator2 = stems.iterator(); iterator2.hasNext();) {
					String stem = (String) iterator2.next();
					System.out.println("Stem --> " + stem);
				}
				List<Suffix> suffixs = wordAnalysis.getSuffixes();
				for (Iterator iterator2 = suffixs.iterator(); iterator2.hasNext();) {
					Suffix suffix = (Suffix) iterator2.next();
					System.out.println("Suffix --> " + suffix.toString());
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
